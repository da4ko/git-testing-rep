
package com.sirma.javacourse.intro.arraysort;

import com.sirma.javacourse.intro.arrayprocc.ArrayProcessor;

/**
 * The Run program implements an application that
 * sorts an array of integers using QuickSort.
 */
public class Run {

	public static void main(String[] args) {
		int arr[] = { 10, 80, 30, 90, 40, 50, 70, 55, 43, 77, 23, 16, 78 };
		int n = arr.length;

		ArrayProcessor.print(arr);
		ArraySort.quickSort(arr, 0, n - 1);
		ArrayProcessor.print(arr);
	}
}
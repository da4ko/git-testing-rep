

package com.sirma.javacourse.intro.arraysort;

/**
 * The ArraySort.quickSort() method sorts an array of integers using the QuickSort method
 */
public class ArraySort {

	static void quickSort(int[] array, int firstIndex, int lastIndex) {
		int[] sortedArray = new int[array.length];

		if (firstIndex < lastIndex) {
			int pi = partition(array, firstIndex, lastIndex);

			quickSort(array, firstIndex, pi - 1);
			quickSort(array, pi + 1, lastIndex);
		}

		return;
	}

	static int partition(int[] array, int firstIndex, int lastIndex) {
		int pivot = array[lastIndex];

		int i = (firstIndex - 1);

		for (int j = firstIndex; j <= lastIndex - 1; j++) {
			if (array[j] <= pivot) {
				i++;
				int swap = array[j];
				array[j] = array[i];
				array[i] = swap;
			}
		}
		int swap = array[i + 1];
		array[i + 1] = array[lastIndex];
		array[lastIndex] = swap;

		return i + 1;
	}

}
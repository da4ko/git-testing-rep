
package com.sirma.javacourse.intro.arrayprocc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Run program implements an application that
 * processes an array.
 * - find index of min element
 * - sum all elements
 * - print the array
 * - find the contiguous subarray which has the largest sum and print the sum
 */
public class Run {

	private static Logger logger = LoggerFactory.getLogger(Run.class);

	public static void main(String[] args) {

		//int[] array = { 45, 34, 65, 34, 23, 45, 88, 66 };
		int[] array = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };

		logger.info("The index of the element with minimum value is " + ArrayProcessor.getMinElementIndex(array));
		logger.info("The sum of all elements in the array is: " + ArrayProcessor.sum(array));
		ArrayProcessor.print(array);
		logger.info("The sum of the contiguous subarray which has the largest sum is: "
				+ ArrayProcessor.findSumContigousSubarray(array));
	}
}

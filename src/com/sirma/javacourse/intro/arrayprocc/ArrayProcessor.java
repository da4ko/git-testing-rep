
package com.sirma.javacourse.intro.arrayprocc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ArrayProcessor is a class used to process an array.
 * int getMinElementIndex(int[]) - returns the index of the minimal element
 * int sum(int[]) - returns the sum of all elements
 * void print(int[]) - prints the array elements to the logger
 * int findSumContigousSubarray(int[]) - finds the contiguous subarray
 * which has the largest sum and print the sum. Using Kadane's algorithm.
 */
public class ArrayProcessor {

	private static Logger logger = LoggerFactory.getLogger(ArrayProcessor.class);

	static int getMinElementIndex(int[] array) {
		int minElementIndex = 0;

		for (int i = 0; i < array.length; i++) {
			if (array[i] < array[minElementIndex]) {
				minElementIndex = i;
			}
		}

		return minElementIndex;
	}

	static int sum(int[] array) {
		int totalSum = 0;

		for (int number : array) {
			totalSum += number;
		}

		return totalSum;
	}

	static void print(int[] array) {
		logger.info("Array: {}", (Object) array);
	}

	static int findSumContigousSubarray(int[] array) {
		int max_sum = 0;
		int max_ending_here = 0;

		for (int i = 0; i < array.length; i++) {
			max_ending_here = max_ending_here + array[i];
			if (max_ending_here < 0) {
				max_ending_here = 0;
			}

			if (max_sum < max_ending_here) {
				max_sum = max_ending_here;
			}
		}
		return max_sum;
	}
}

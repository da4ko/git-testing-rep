
package com.sirma.javacourse.intro.lcm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Run program implements an application that
 * calculates least common multiple of two integers.
 */
public class Run {

	static Logger logger = LoggerFactory.getLogger(Run.class);

	public static void main(String[] args) {

		int firstNumber = 0;
		int secondNumber = 0;

		if (args.length == 0) {
			firstNumber = 4544;
			secondNumber = 224;
		} else if (args.length == 2) {
			firstNumber = Integer.parseInt(args[0]);
			secondNumber = Integer.parseInt(args[0]);
		} else {
			System.out.println("Usage: run int int");
		}

		logger.info("Least common multiple of " + firstNumber + " and " + secondNumber
				+ " is: " + CalculatorLCM.calculate(firstNumber, secondNumber));
	}
}

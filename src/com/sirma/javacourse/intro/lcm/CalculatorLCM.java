package com.sirma.javacourse.intro.lcm;

import com.sirma.javacourse.intro.gcd.CalculatorGCD;

/**
 * CalculatorLCM is a class used to calculate least
 * common multiple of two integers
 */
public class CalculatorLCM {

	/**
	 * Returns an int which is the least common divisor of
	 * two integers using the greatest common divisor method
	 *
	 * @param firstNumber - integer
	 * @param secondNumber - integer
	 * @return the least common multiple of the specified numbers
	 */
	public static int calculate(int firstNumber, int secondNumber) {

		return (firstNumber * secondNumber) / CalculatorGCD.calculate(firstNumber, secondNumber);
	}
}

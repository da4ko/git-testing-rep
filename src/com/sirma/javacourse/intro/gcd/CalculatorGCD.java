package com.sirma.javacourse.intro.gcd;

/**
 * CalculatorGCD is a class used to calculate greatest
 * common divisor of two integers
 */
public class CalculatorGCD {

	/**
	 * Returns an int which is the greatest common divisor
	 * of two integers
	 *
	 * @param firstNumber - integer
	 * @param secondNumber - integer
	 * @return the greatest common divisor of the specified numbers
	 */
	static int calculate(int firstNumber, int secondNumber) {
		while (firstNumber != secondNumber) {
			if (firstNumber < secondNumber) {
				secondNumber -= firstNumber;
			} else {
				firstNumber -= secondNumber;
			}
		}
		return firstNumber;
	}
}

package com.sirma.javacourse.intro.gcd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Run program implements an application that
 * calculates greatest common divisor of two integers.
 */
public class Run {

	private static Logger logger = LoggerFactory.getLogger(Run.class);

	public static void main(String[] args) {

		int firstNumber = 0;
		int secondNumber = 0;

		if (args.length == 0) {
			firstNumber = 4544;
			secondNumber = 224;
		} else if (args.length == 2) {
			firstNumber = Integer.parseInt(args[0]);
			secondNumber = Integer.parseInt(args[0]);
		} else {
			System.out.println("Usage: run int int");
		}

		logger.info("The greatest common divisor of " + firstNumber +
				" and " + secondNumber + " is "
				+ CalculatorGCD.calculate(firstNumber, secondNumber));
	}
}

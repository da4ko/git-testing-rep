/*
 * Project: Introducing Java
 * Filename: run.java
 *
 * Part of Java Course at Sirma Solutions.
 * Author: Alexandrov
 *
 * Copyright (c) 2018.
 */

package com.sirma.javacourse.intro.arrayreverse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sirma.javacourse.intro.arrayproccess.ArrayProcessor;

/**
 * The Run program implements an application that
 * reverses a given array of integers.
 */
public class Run {

	private static Logger logger = LoggerFactory.getLogger(Run.class);

	public static void main(String args[]) {

		int arr[] = { 10, 80, 30, 90, 50, 44 };

		ArrayProcessor.print(arr);
		ArrayReverse.reverse(arr);
		ArrayProcessor.print(arr);

	}


}

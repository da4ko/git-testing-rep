
package com.sirma.javacourse.intro.arrayreverse;

/**
 * The ArrayReverse class is used to reverse an array of integers.
 */
public class ArrayReverse {

	static void reverse(int[] array) {
		for (int i = 0; i < (array.length / 2); i++) {
			int temp = array[i];
			array[i] = array[(array.length - 1) - i];
			array[(array.length - 1) - i] = temp;
		}
	}
}
